function postCardPayment(uid, name, email, card, cvv, expDate, type, amnt) {
    var http = require('http')

    var data = JSON.stringify({
        uid: uid,
        customerName: name,
        customerEmail: email,
        cardNumber: card,
        cardCvv: cvv,
        cardExpireDate: expDate,
        cardType: type,
        amount: amnt
    })

    const options = {
        host: 'localhost',
        port: 8761,
        path: "/api/payments",
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Content-Length": Buffer.byteLength(data)
        }
    }

    const req = http.request(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)

        res.on('data', (d) => {
            process.stdout.write(d)
        })
    })

    req.on('error', (error) => {
        console.error(error)
    })

    req.write(data)
    req.end()
}

function postAchPayment(uid, name, email, routing, account, amnt) {
    var http = require('http')

    var data = JSON.stringify({
        uid: uid,
        customerName: name,
        customerEmail: email,
        routingNumber: routing,
        accountNumber: account,
        amount: amnt
    })

    const options = {
        host: 'localhost',
        port: 8761,
        path: "/api/payments",
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Content-Length": Buffer.byteLength(data)
        }
    }

    const req = http.request(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)

        res.on('data', (d) => {
            process.stdout.write(d)
        })
    })

    req.on('error', (error) => {
        console.error(error)
    })

    req.write(data)
    req.end()
}

postCardPayment("232321", "ops", "o@g.com", "242424242424242", "883", "0822", "credit", "210")

postAchPayment("232321", "ops", "o@g.com", "021000021", "111111111", "210")
